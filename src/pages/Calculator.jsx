import React, { useState } from "react"
import { Form } from "react-bootstrap";
import { useContent } from "react-ssg";
import { Layout } from "../components/Layout.jsx";
import { SEO } from "../components/SEO.jsx";

const objFromKeyValue = (ar) => {
  const res = {};
  ar.forEach(([a, b]) => res[a] = b);
  return res;
};

const defObj = (params) => objFromKeyValue(params.map((x) => [x.name, x.default]));

const describeFormule = (names, exp) => {
  const f = (s) => {
    if (typeof s === 'number') return s;
    if (typeof s === 'string') return names[s];
    if (s.type === 'multiple') {
      return `(${s.op.map(f).join(' * ')})`;
    }
    if (s.type === 'sum') {
      return `(${s.op.map(f).join(' + ')})`;
    }
  };
  return f(exp);
}

const compute = (state, variables) => {
  const res = { ...state };
  const f = (s) => {
    if (typeof s === 'number') return s;
    if (typeof s === 'string') return res[s];
    if (s.type === 'multiple') {
      return s.op.map(f).reduce((a, b) => a * b);
    }
    if (s.type === 'sum') {
      return s.op.map(f).reduce((a, b) => a + b);
    }
  };
  variables.forEach((k) => {
    res[k.name] = f(k.value);
  });
  return res;
};

export const Calculator = () => {
  const data = useContent('/calculator.yml');
  const [state, setState] = useState(defObj(data.params));
  const cnc = (p, v) => {
    if (p.type === 'range') {
      setState({
        ...state,
        [p.name]: Number(Math.max(Math.min(v, p.max), p.min)),
      });
    } else {
      setState({
        ...state,
        [p.name]: Number(v),
      });
    }
  };
  const cng = (p) => (e) => {
    cnc(p, e.target.value);
  };
  const computed = compute(state, data.computed);
  const names = objFromKeyValue([
    ...data.params.map((x)=>[x.name, x.label]),
    ...data.computed.map((x)=>[x.name, x.label]),
  ]);
  return (
    <Layout>
      <SEO title={data.title} />
      <h1>{data.title}</h1>
      <p>{data.description}</p>
      <Form>
      {data.params.map((p) => (
        <Form.Group key={p.name}>
          <Form.Label>{p.label}: {state[p.name]}</Form.Label>
          {p.type === 'range' && (
            <Form.Control
              type="range"
              onChange={cng(p)}
              max={p.max}
              min={p.min}
              value={state[p.name]}
            />
          )}
          {p.type === 'radio' && (
            <div className="mb-3">
              {p.options.map((o)=>(
                <Form.Check
                  inline type="radio" label={o} onChange={()=>cnc(p, o)}
                  name={p.name}
                />
              ))}
            </div>
          )}
        </Form.Group>
      ))}
      </Form>
      <h2>{data.words.finalPrice}: {computed.allPrice.toLocaleString('fa')}</h2>
      <h2>{data.words.details}:</h2>
      {data.computed.map((c)=>(
        <li key={c.name}>
          {c.label}: {describeFormule(names, c.value)}
          <br/>
          {c.label} = {computed[c.name].toLocaleString('fa')} {c.unit}
        </li>
      ))}
    </Layout>
  );
};
