const removeMD = (x)=>{
  if (x.endsWith('.md')) {
    return x.slice(0, -3) + "/";
  }
};

export const urls = (data) => [
  "/", "/about/", "/faq/", "/blogs/", "/calculator/",
  ...Object.keys(data).filter((x)=>x.startsWith('/blogs/')).map(removeMD),
];
